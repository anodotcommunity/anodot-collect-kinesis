VIRTUALENV=lambda_venv
PYTHON=python3.6
cd $(dirname $0)
UPLOAD_FILE=$PWD/upload.zip
rm -rf $VIRTUALENV
virtualenv --python=$PYTHON $VIRTUALENV
source $VIRTUALENV/bin/activate
pip install -r requirements.txt

# clean zip
rm $UPLOAD_FILE
# build zip
( cd $VIRTUALENV/lib/python3.6/site-packages && find -name \*.pyc -exec rm {} \; && zip -r9 $UPLOAD_FILE * )
zip -g9 $UPLOAD_FILE lambda_function.py
