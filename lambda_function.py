import requests
import os
import base64
import json
import dateutil.parser
from datetime import datetime
from anodot_api import make_sample, AnodotAPISession
import logging


ANODOT_ENDPOINT = os.environ.get("ANODOT_ENDPOINT")
ANODOT_TOKEN = os.environ.get("ANODOT_TOKEN")
CONST_DIMS = os.environ.get("CONST_DIMS") or "ver=1"
DIMENSION_COMBINATIONS = [ comb.split(',') for comb in os.environ.get("DIMENSION_COLS").split('|') ]
METRICS = os.environ.get("METRIC_COLS").split(',')
TIMESTAMP = os.environ.get("TIMESTAMP_COL")
DEBUG_REQUESTS = os.environ.get("DEBUG_REQUESTS")
SEND_COLLECTOR_STATS = os.environ.get("SEND_COLLECTOR_STATS")

if DEBUG_REQUESTS:
    import http.client
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True
    http.client.HTTPConnection.debuglevel = 1

logging.basicConfig(level = logging.DEBUG, stream = 'ext://sys.stdout')
session = AnodotAPISession( token = ANODOT_TOKEN, env_or_root = ANODOT_ENDPOINT )

#DIMENSIONS = ["eventType", "productType"]
#METRICS    = ["eventCount", "productPriceSum"]
#TIMESTAMP  = "eventTimeWindow"

CONST_DIMS_PAIRS = [ tuple(pair.split('=')[:2]) for pair in CONST_DIMS.strip().split('.') ]

def convert_record_to_samples(record):
    for dims in DIMENSION_COMBINATIONS:
        gb = '-'.join(dims)
        properties = { k: record.get(k,'unknown') for k in dims }
        for m in METRICS:
            if m not in record: continue
            yield make_sample(
                    value = record[m],
                    property_pairs =  CONST_DIMS_PAIRS + [ ( "what", m ), ("groupedBy", gb) ] + list(properties.items()),
                    dttm = dateutil.parser.parse(record[TIMESTAMP], ignoretz = True)
                    )

def convert_kinesis_records_to_samples(event):
    event_records = None
    event_records_len = 0
    if 'Records' in event:
        print(f"Call to Lambda function from kinesis stream")
        try:
            event_records = (( json.loads(base64.b64decode(rec['kinesis']['data']).decode(), strict = False) for rec in event['Records'] ))
            event_records_len = len(event['Records'])
        except Exception as e:
            print(f"Stream - Caught exception {e}")
            raise
    elif 'records' in event:
        print(f"Direct call to Lambda function from kinesis analytics")
        try:
            event_records = (( json.loads(base64.b64decode(rec['data']).decode(), strict = False) for rec in event['records'] ))
            event_records_len = len(event['records'])
        except Exception as e:
            print(f"Direct - Caught exception {e}")
            raise
    else:
        logging.getLogger().critical("Unknown event format")
        raise Exception("Unknown event format")
    
    converted = [ sample for record in event_records for sample in convert_record_to_samples(record) ]
    if SEND_COLLECTOR_STATS:
        if converted:
            approx_dttm = datetime.utcfromtimestamp(converted[0]["timestamp"])
            converted += [ 
                    make_sample( 
                                value = len(converted), 
                                property_pairs = CONST_DIMS_PAIRS + [ ("what", "output_samples"), ("collector", "anodot-kinesis-lambda") ],
                                dttm = approx_dttm
                    ),
                    
                    make_sample( 
                                value = event_records_len, 
                                property_pairs = CONST_DIMS_PAIRS + [ ("what", "input_records"), ("collector", "anodot-kinesis-lambda") ],
                                dttm = approx_dttm
                    ),
                   
                    ]

    print(f"converted {len(converted)} records to Anodot format")
    return converted

def lambda_handler(event, context):
    print(f"got event with keys: {json.dumps(list(event.keys()))}")
    samples = list(convert_kinesis_records_to_samples(event))
    print(f'got timestamps: {set([ e["timestamp"] for e in samples ])}')
    print(f'now: {datetime.now()}, utcnow: {datetime.utcnow()}')
    session.post_samples(samples)


